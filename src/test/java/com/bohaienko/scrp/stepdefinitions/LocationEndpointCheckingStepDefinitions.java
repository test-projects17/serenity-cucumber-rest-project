package com.bohaienko.scrp.stepdefinitions;

import com.bohaienko.scrp.steps.LocationEndpoint;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

import java.util.List;
import java.util.Map;

import static com.bohaienko.scrp.Endpoints.LOCATION;
import static net.serenitybdd.core.Serenity.setSessionVariable;
import static net.serenitybdd.rest.SerenityRest.*;
import static org.hamcrest.Matchers.containsStringIgnoringCase;

public class LocationEndpointCheckingStepDefinitions {

	@Steps
	private LocationEndpoint locationEndpoint;

	private String targetUrl;

	@Given("the customer searches station {string} by name")
	public void set_url_for_name(String name) {
		targetUrl = LOCATION.getUrl() + name;
		given();
	}

	@Given("the customer obtains station ID by name {string}")
	public void obtain_station_id_by_name(String name) {
		set_url_for_name(name);
		list_stations();
		setSessionVariable("GlobalStationId")
				.to(locationEndpoint.getStationIdByStationNameFromLastResponse(name));
	}

	@When("the customer looks up the needle station")
	public void list_stations() {
		get(targetUrl);
	}

	@Then("the API should return the list of stations with the {string} value in it")
	public void check_list_contains(String expectedMessage) {
		restAssuredThat(lastResponse -> lastResponse
				.statusCode(200)
				.body(containsStringIgnoringCase(expectedMessage)));
	}

	@Then("the API should return the list of stations with the corresponding IDs for names:")
	public void check_stations_ids_for_names(List<Map<String, String>> stations) {
		stations.forEach(s ->
				restAssuredThat(lastResponse -> locationEndpoint
						.getStationIdByStationNameFromLastResponse(s.get("name"))
						.equals(Integer.parseInt(s.get("ID")))));
	}
}
