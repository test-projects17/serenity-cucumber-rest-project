package com.bohaienko.scrp.stepdefinitions;

import io.cucumber.java.en.Then;
import net.thucydides.core.annotations.DefaultUrl;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static net.serenitybdd.rest.SerenityRest.restAssuredThat;

public class CommonStepDefinitions {
	@Then("the API should return the response of {string} JSON schema")
	public void api_return_response_of_schema(String schemaFileName) {
		restAssuredThat(lastResponse -> lastResponse
				.statusCode(200)
				.body(matchesJsonSchemaInClasspath("schemas/" + schemaFileName)));
	}

	@Then("the API should return the status code {int}")
	public void api_return_status_of_code(Integer statusCode) {
		restAssuredThat(lastResponse -> lastResponse
				.statusCode(statusCode));
	}
}
