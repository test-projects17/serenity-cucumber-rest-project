package com.bohaienko.scrp.stepdefinitions;

import com.bohaienko.scrp.steps.JourneyDetailsEndpoint;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

import java.util.UUID;

import static com.bohaienko.scrp.Endpoints.JOURNEY_DETAILS;
import static net.serenitybdd.core.Serenity.getCurrentSession;
import static net.serenitybdd.rest.SerenityRest.*;

public class JourneyDetailsEndpointCheckingStepDefinitions {

	@Steps
	private JourneyDetailsEndpoint journeyDetailsEndpoint;

	private String targetUrl;
	private String globalDepartureId;

	@Given("the customer searches departure details with obtained details ID")
	public void set_url_for_details_id() {
		globalDepartureId = getCurrentSession().get("GlobalDepartureDetailsId").toString();
		targetUrl = JOURNEY_DETAILS.getUrl() + globalDepartureId;
		given();
	}

	@Given("the customer searches departure details with random details ID")
	public void set_url_for_random_details_id() {
		globalDepartureId = UUID.randomUUID().toString();
		targetUrl = JOURNEY_DETAILS.getUrl() + globalDepartureId;
		given();
	}

	@When("the customer looks up the needle journey details")
	public void list_stations() {
		when().get(targetUrl);
	}

	@Then("the API should return the list of journey details with train name {string} in each")
	public void check_list_contains(String trainName) {
		restAssuredThat(lastResponse -> journeyDetailsEndpoint
				.getJourneyDetailsStreamFromLastResponse()
				.allMatch(e -> e.getTrain().equals(trainName)));
	}

	@Then("the API should return the list of journey details with {string} station in it")
	public void check_departures_are_unique(String stationName) {
		restAssuredThat(lastResponse -> journeyDetailsEndpoint
				.getJourneyDetailsStreamFromLastResponse()
				.anyMatch(e -> e.getStopName().equals(stationName)));
	}
}
