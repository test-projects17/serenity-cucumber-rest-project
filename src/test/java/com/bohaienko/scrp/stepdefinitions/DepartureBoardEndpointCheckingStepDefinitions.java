package com.bohaienko.scrp.stepdefinitions;

import com.bohaienko.scrp.model.Departure;
import com.bohaienko.scrp.steps.DepartureBoardEndpoint;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

import java.util.Arrays;

import static com.bohaienko.scrp.Endpoints.DEPARTURE_BOARD;
import static junit.framework.TestCase.assertEquals;
import static net.serenitybdd.core.Serenity.getCurrentSession;
import static net.serenitybdd.core.Serenity.setSessionVariable;
import static net.serenitybdd.rest.SerenityRest.*;

public class DepartureBoardEndpointCheckingStepDefinitions {

	@Steps
	private DepartureBoardEndpoint departureBoardEndpoint;

	private String targetUrl;
	private String stationId;

	@Given("the customer searches the departure board by found station ID and date {string}")
	public void set_url_for_date(String date) {
		stationId = getCurrentSession().get("GlobalStationId").toString();
		targetUrl = DEPARTURE_BOARD.getUrl() + stationId;
		given().param("date", date);
	}

	@Given("the customer searches the departure board by found station ID and without date")
	public void set_url_for_no_date() {
		stationId = getCurrentSession().get("GlobalStationId").toString();
		targetUrl = DEPARTURE_BOARD.getUrl() + stationId;
		given();
	}

	@Given("the customer obtains departure's details ID by train name {string}")
	public void obtain_departure_details_id_by_name(String name) {
		list_stations();
		setSessionVariable("GlobalDepartureDetailsId")
				.to(departureBoardEndpoint.getDepartureDetailsIdByNameFromLastResponse(name));
	}

	@When("the customer looks up the needle departure board")
	public void list_stations() {
		when().get(targetUrl);
	}

	@Then("the API should return the list of departures with the found station ID in each")
	public void check_list_contains() {
		restAssuredThat(lastResponse -> Arrays.stream(lastResponse
				.statusCode(200)
				.extract().as(Departure[].class))
				.allMatch(e -> e.getBoardId().toString().equals(stationId)));
	}

	@Then("the API should return the list of departures that are all unique")
	public void check_departures_are_unique() {
		assertEquals(
				"The target endpoint's response contains not unique values",
				departureBoardEndpoint.getDeparturesStreamFromLastResponse().count(),
				departureBoardEndpoint.getDeparturesStreamFromLastResponse().distinct().count()
		);
	}
}
