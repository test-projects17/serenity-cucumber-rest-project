package com.bohaienko.scrp.config;

import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import org.junit.Assume;

public class CucumberHooks {
	@Before("@ignore")
	public void ignore(Scenario scenario){
		System.out.println("SKIP SCENARIO: " + scenario.getName());
		Assume.assumeTrue(false);
	}
}
