package com.bohaienko.scrp.steps;

import com.bohaienko.scrp.model.Departure;
import com.bohaienko.scrp.model.Station;

import java.util.Arrays;
import java.util.stream.Stream;

import static net.serenitybdd.rest.SerenityRest.lastResponse;

public class DepartureBoardEndpoint {
	public String getDepartureDetailsIdByNameFromLastResponse(String needleName) {
		return getDeparturesStreamFromLastResponse()
				.filter(e -> e.getName().toLowerCase().equals(needleName.toLowerCase()))
				.findFirst()
				.orElseThrow(NullPointerException::new)
				.getDetailsId();
	}

	public Stream<Departure> getDeparturesStreamFromLastResponse() {
		return Arrays.stream(lastResponse()
				.body()
				.as(Departure[].class));
	}
}
