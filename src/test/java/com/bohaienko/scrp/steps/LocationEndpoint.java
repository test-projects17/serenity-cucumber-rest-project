package com.bohaienko.scrp.steps;

import com.bohaienko.scrp.model.Station;

import java.util.Arrays;

import static net.serenitybdd.rest.SerenityRest.lastResponse;

public class LocationEndpoint {
	public Integer getStationIdByStationNameFromLastResponse(String needleName) {
		return Arrays.stream(lastResponse().body().as(Station[].class))
				.filter(e -> e.getName().toLowerCase().equals(needleName.toLowerCase()))
				.findFirst()
				.orElseThrow(NullPointerException::new)
				.getId();
	}
}
