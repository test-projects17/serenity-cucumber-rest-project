package com.bohaienko.scrp.steps;

import com.bohaienko.scrp.model.JourneyDetail;

import java.util.Arrays;
import java.util.stream.Stream;

import static net.serenitybdd.rest.SerenityRest.lastResponse;

public class JourneyDetailsEndpoint {
	public Stream<JourneyDetail> getJourneyDetailsStreamFromLastResponse() {
		return Arrays.stream(lastResponse()
				.body()
				.as(JourneyDetail[].class));
	}
}
