package com.bohaienko.scrp;

public enum Endpoints {
	BASE("https://api.deutschebahn.com/freeplan/v1/"),
	LOCATION(BASE.getUrl() + "location/"),
	DEPARTURE_BOARD(BASE.getUrl() + "departureBoard/"),
	JOURNEY_DETAILS(BASE.getUrl() + "journeyDetails/");

	private final String url;

	Endpoints(String url) {
		this.url = url;
	}

	public String getUrl() {
		return url;
	}
}
