package com.bohaienko.scrp.model;

import lombok.*;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class JourneyDetail {
	Integer stopId;
	String stopName;
	String lat;
	String lon;
	String arrTime;
	String depTime;
	String train;
	String type;
	String operator;
	List<Note> notes;
}
