package com.bohaienko.scrp.model;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class Station {
	String name;
	Float lon;
	Float lat;
	Integer id;
}
