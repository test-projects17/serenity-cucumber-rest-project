package com.bohaienko.scrp.model;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class Note {
	String key;
	String priority;
	String text;
}
