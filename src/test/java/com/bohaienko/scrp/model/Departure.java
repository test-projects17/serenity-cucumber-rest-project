package com.bohaienko.scrp.model;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class Departure {
	String name;
	String type;
	Integer boardId;
	Integer stopId;
	String stopName;
	String dateTime;
	String track;
	String detailsId;
}
