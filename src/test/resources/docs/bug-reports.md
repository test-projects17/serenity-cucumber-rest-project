|Property|Value|
|----|----| 
|Bug ID|1|
|Bug Summary:|GET request on location returns 500|
|Bug Description:|No validations set for route params of not expected value|
|Severity:|Medium|
|Steps to reproduce:|Send GET request to the url: https://api.deutschebahn.com/freeplan/v1/location/route_param, where `route_param` is a string of digits, e.g., `123456`|
|Actual result:|Status code 500|
|Expected result:|Status code 400|
|----|----|
|Bug ID|2|
|Bug Summary:|GET request on location returns 500|
|Bug Description:|No validations set for not suitable route params|
|Severity:|Medium|
|Steps to reproduce:|Send GET request to the url: https://api.deutschebahn.com/freeplan/v1/journeyDetails/route_param, , where `route_param` is a random string, e.g., random UUID|
|Actual result:|Status code 500|
|Expected result:|Status code 400|
