The tested application, **Fahrplan-Free - Deutsche Bahn**, offers its customers open source [API](https://any-api.com/deutschebahn_com/fahrplan/console), which gives an opportunity to check the schedule details of railway journeys.

The API does not require authorization and consists of 4 routes:
- **GET** /arrivalBoard/{id}, which uses `stationId` value for route param `id`, `date` value in format `mm/dd/yyyy` for query param and returns a JSON message like:
```
[
    {
        "name": "Augsburg Hbf",
        "lon": 10.885568,
        "lat": 48.365444,
        "id": 8000013
    },
    {
        "name": "Aachen Hbf",
        "lon": 6.091495,
        "lat": 50.767803,
        "id": 8000001
    }
]
``` 
- **GET** /departureBoard/{id}, which uses `stationId` value for route param `id`, `date` value in format `mm/dd/yyyy` for query param and returns a JSON message like:
```
[
    {
        "name": "ICE 1545",
        "type": "ICE",
        "boardId": 8000001,
        "stopId": 8000001,
        "stopName": "Aachen Hbf",
        "dateTime": "2021-01-27T07:08",
        "track": "6",
        "detailsId": "41925%2F14818%2F779600%2F375825%2F80%3fstation_evaId%3D8000001"
    },
    {
        "name": "ICE 11",
        "type": "ICE",
        "boardId": 8000001,
        "stopId": 8000001,
        "stopName": "Aachen Hbf",
        "dateTime": "2021-01-27T07:39",
        "track": "9",
        "detailsId": "817005%2F272829%2F653302%2F54316%2F80%3fstation_evaId%3D8000001"
    }
]
``` 
- **GET** /journeyDetails/{id}, which uses `detailsId` value for route param `id` and returns a JSON message like:
```
[
    {
        "name": "ICE 11",
        "type": "ICE",
        "boardId": 8000001,
        "stopId": 8000001,
        "stopName": "Aachen Hbf",
        "dateTime": "2021-01-27T07:36",
        "origin": "Bruxelles Midi",
        "track": "9",
        "detailsId": "111993%2F37825%2F989190%2F457264%2F80%3fstation_evaId%3D8000001"
    },
    {
        "name": "ICE 18",
        "type": "ICE",
        "boardId": 8000001,
        "stopId": 8000001,
        "stopName": "Aachen Hbf",
        "dateTime": "2021-01-27T08:16",
        "origin": "Frankfurt&#x0028;Main&#x0029;Hbf",
        "track": "9",
        "detailsId": "977712%2F327470%2F487632%2F82088%2F80%3fstation_evaId%3D8000001"
    }
]
``` 
- **GET** /location/{name}, which uses `stopName` value for route param `name` and returns a JSON message like:
```
[
    {
        "stopId": 8800004,
        "stopName": "Bruxelles Midi",
        "lat": "50.835375",
        "lon": "4.335695",
        "depTime": "06:24",
        "train": "ICE 11",
        "type": "ICE",
        "operator": "DB",
        "notes": [
            {
                "key": "ZN",
                "priority": "-1",
                "text": "ICE International"
            },
            {
                "key": "PB",
                "priority": "200",
                "text": "Obligation to cover nose and mouth"
            },
            {
                "key": "CK",
                "priority": "200",
                "text": "Komfort Check-in possible (visit bahn.de/kci for more information)"
            },
            {
                "key": "BR",
                "priority": "450",
                "text": "Bordrestaurant"
            }
        ]
    },
    {
        "stopId": 8800002,
        "stopName": "Bruxelles-Nord",
        "lat": "50.860239",
        "lon": "4.361458",
        "arrTime": "06:30",
        "depTime": "06:32",
        "train": "ICE 11",
        "type": "ICE",
        "operator": "DB",
        "notes": [
            {
                "key": "ZN",
                "priority": "-1",
                "text": "ICE International"
            },
            {
                "key": "PB",
                "priority": "200",
                "text": "Obligation to cover nose and mouth"
            },
            {
                "key": "CK",
                "priority": "200",
                "text": "Komfort Check-in possible (visit bahn.de/kci for more information)"
            },
            {
                "key": "BR",
                "priority": "450",
                "text": "Bordrestaurant"
            }
        ]
    }
]
``` 
