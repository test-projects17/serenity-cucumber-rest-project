Feature: Journey details endpoint negative checking

  @ignore #possible bug #2
  @func
  Scenario: Fail search journey details by random journey details ID
    Given the customer searches departure details with random details ID
    When the customer looks up the needle journey details
    Then the API should return the status code 400
