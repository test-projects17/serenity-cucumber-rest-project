Feature: Departure board endpoint negative checking

  @func
  Scenario: Fail search departure board without date
    Given the customer obtains station ID by name "Berlin"
    And the customer searches the departure board by found station ID and without date
    When the customer looks up the needle departure board
    Then the API should return the status code 400
