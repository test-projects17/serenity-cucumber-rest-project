Feature: Location endpoint checking

  @smoke
  Scenario: Success search station by name
    Given the customer searches station "Berlin" by name
    When the customer looks up the needle station
    Then the API should return the list of stations with the "Berlin" value in it

  @contract
  Scenario: Success search station response comply with the contract
    Given the customer searches station "Berlin" by name
    When the customer looks up the needle station
    Then the API should return the response of "location.json" JSON schema

  @func
  Scenario: Success search station by name
    Given the customer searches station "Berlin" by name
    And the customer obtains station ID by name "Berlin"
    When the customer looks up the needle station
    Then the API should return the list of stations with the corresponding IDs for names:
      | name              | ID      |
      | Berlin            | 8096003 |
      | Berlin Hbf        | 8011160 |
      | Berlin Ostbahnhof | 8010255 |

  @func
  Scenario: Success search station by name with diacritic symbols in it
    Given the customer searches station "éèêëçñøð" by name
    When the customer looks up the needle station
    Then the API should return the status code 200

  @ignore #possible bug #1
  @func
  Scenario: Fail search station by name consist of digits
    Given the customer searches station "123456" by name
    When the customer looks up the needle station
    Then the API should return the status code 400
