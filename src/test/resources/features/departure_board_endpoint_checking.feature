Feature: Departure board endpoint checking

  Background: Set up departure board look up
    Given the customer obtains station ID by name "Berlin"
    And the customer searches the departure board by found station ID and date "01/31/2021"
    When the customer looks up the needle departure board

  @smoke
  Scenario: Success search departure board by station ID and date
    Then the API should return the list of departures with the found station ID in each

  @contract
  Scenario: Success search departure board response comply with the contract
    Then the API should return the response of "departureBoard.json" JSON schema

  @func
  Scenario: Success search departure board shows only unique departures
    Then the API should return the list of departures that are all unique
