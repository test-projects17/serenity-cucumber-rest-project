Feature: Journey details endpoint checking

  Background: Set up journey details look up
    Given the customer obtains station ID by name "Berlin"
    And the customer searches the departure board by found station ID and date "01/31/2021"
    And the customer obtains departure's details ID by train name "ICE 948"
    And the customer searches departure details with obtained details ID
    When the customer looks up the needle journey details

  @smoke
  Scenario: Success search journey details by journey details ID contains needle train name
    Then the API should return the list of journey details with train name "ICE 948" in each

  @contract
  Scenario: Success search journey details response comply with the contract
    Then the API should return the response of "journeyDetails.json" JSON schema

  @func
  Scenario: Success search journey details by journey details ID contains initial needle station name
    Then the API should return the list of journey details with "Berlin" station in it
