# Sample Serenity Cucumber Rest API Project
 
### Prerequisites
- **git >= 2.27.0**
- **openjdk >= 11.0.2**;
- **Apache Maven >= 3.6.3**;

## Build
- Clone project and enter the project root directory
```
git clone https://gitlab.com/test-projects17/serenity-cucumber-rest-project.git && cd serenity-cucumber-rest-project/
```
- Exec
```
mvn clean install -DskipITs
```
 
## Run
- To run tests execute in parent dir:
```
mvn clean verify 
```

### Run options:
- To run test group, specify option `-Dcucumber.filter.tags="@<group_name>"` with the previous command, e.g.:
```
mvn verify -Dcucumber.filter.tags="@smoke"
```

#### Available groups:
- `@smoke` - "smoke" test group 
- `@contract` - tests checking messages compliance, i.e., contracts
- `@func` - functional tests

### Docs

Please, refer to the next supporting documents:
- Target application endpoints [description](src/test/resources/docs/route-descriptions.md)
- Target application testing [bug-reports](src/test/resources/docs/bug-reports.md)

### Configuration
Logging level could be configured in `src/test/resources/logback-test.xml` file

### Ports exposed by component
The component does not need to be exposed

### Reporting
The generated Serenity BDD HTML report will be accessible after test run by path:
`target/site/serenity/index.html`

Within the Gitlab CI environment the test report is generated during the each hooked on commit pipeline and belongs to its artifacts. 

### Project structure
```
src
  + main
  + test
    + java
      + com.bohaienko.scrp
        + model
        + stepdefinitions      
        + steps
        CucumberTestSuite.java
        Endpoints.java        
    + resources
      + features
      + schemas                
```
### Adding new tests
Keeping in mind the above-mentioned project structure, the process of adding new tests is next:
1. Add a new feature file in `src/test/resources/features` dir or update the existing one with new scenarios according to the official Cucumber [guides](https://cucumber.io/docs/guides/).
2. Implement steps in `src/test/java/com/bohaienko/scrp/stepdefinitions` dir, considering the Serenity BDD [guide](https://serenity-bdd.github.io/theserenitybook/latest/serenity-rest.html) and the RestAssured [guide](https://github.com/serenity-bdd/serenity-rest-starter/blob/master/src/test/java/starter/stepdefinitions/TradeStepDefinitions.java).
3. To make your code in `stepdefinitions` more neat and brief, use the abstraction approach with the detail implementation of logic in the `steps` dir.
4. Use `Endpoints` enum file to gather all REST API endpoints in one place.
5. Add more .json files with the JSON schema descriptions into `src/test/resources/schemas` dir to check more responses on contracts compliance.
6. Use "model objects" [approach](http://www.javapractices.com/topic/TopicAction.do?Id=187) to describe and work with messages or other entities in a more comfortable and agile way. Some examples are already stored in `src/test/java/com/bohaienko/scrp/models` dir
7. `CucumberTestSuite` is the entry point of the test automation framework.

 
